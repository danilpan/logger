package logger

type LogModel struct {
	RequestID string `json:"request_id,omitempty"`
	Key string `json:"key,omitempty"`
	Message interface{} `json:"message,omitempty"`
}

// RequestLoggerModel for logging request
type RequestLoggerModel struct {
	RequestID     string      `json:"request_id,omitempty"`
	RequestBody   interface{} `json:"request_body,omitempty"`
	RequestMethod string      `json:"request_method,omitempty"`
	RequestURI    string      `json:"request_uri,omitempty"`
	System        *System     `json:"system,omitempty"`
	MethodName    string      `json:"method_name,omitempty"`
}

// ForeignRequestLoggerModel for logging foreign request to services
type ForeignRequestLoggerModel struct {
	RequestID     string      `json:"request_id,omitempty"`
	RequestBody   interface{} `json:"request_body,omitempty"`
	RequestMethod string      `json:"request_method,omitempty"`
	MethodName    string      `json:"method_name,omitempty"`
	RequestURL    string      `json:"url,omitempty"`
}

// ForeignResponseLoggerModel for logging foreign response from services
type ForeignResponseLoggerModel struct {
	RequestID      string      `json:"request_id,omitempty"`
	ResponseBody   interface{} `json:"response_body,omitempty"`
	ResponseStatus int         `json:"status,omitempty"`
}

// ResponseLoggerModel for logging response
type ResponseLoggerModel struct {
	RequestID      string      `json:"request_id,omitempty"`
	ResponseBody   interface{} `json:"response_body,omitempty"`
	ResponseStatus int         `json:"response_status,omitempty"`
}

// System for storing info about remote system
type System struct {
	RemoteAddr string `json:"remote_addr,omitempty"`
	Origin     string `json:"origin,omitempty"`
}

func CreateForeignResponseLoggerModel(requestId string, status int, body interface{}) *ForeignResponseLoggerModel {
	f := &ForeignResponseLoggerModel{
		RequestID:      requestId,
		ResponseStatus: status,
	}
	if body != nil {
		f.ResponseBody = body
	}
	return f
}

func CreateForeignRequestLoggerModel(requestID, requestMethod, methodName, url string, body interface{}) *ForeignRequestLoggerModel {
	f := &ForeignRequestLoggerModel{
		RequestID:     requestID,
		RequestMethod: requestMethod,
		MethodName:    methodName,
		RequestURL:    url,
	}
	if body != nil {
		f.RequestBody = body
	}
	return f
}

func CreateResponseLoggerModel(requestID string, body interface{}, status int) *ResponseLoggerModel {
	r := &ResponseLoggerModel{
		RequestID:      requestID,
		ResponseStatus: status,
	}
	if body != nil {
		r.ResponseBody = body
	}
	return r
}

func CreateRequestLoggerModel(requestID, requestUri, requestMethod, remoteAddr, origin, serviceName string, body interface{}) *RequestLoggerModel {
	reqModel := &RequestLoggerModel{
		RequestID:     requestID,
		RequestMethod: requestMethod,
		RequestURI:    requestUri,
		System: &System{
			RemoteAddr: remoteAddr,
			Origin:     origin,
		},
		MethodName: serviceName,
	}
	if body != nil {
		reqModel.RequestBody = body
	}
	return reqModel
}
