package logger

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	lCtx "gitlab.com/danilpan/logger/context"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"strings"
	"time"
)

type Level int
type Output int

// String prefix defined by level enum
var m = map[Level]string{
	TRACE: "TRACE",
	DEBUG: "DEBUG",
	INFO:  "INFO",
	WARN:  "WARN",
	ERROR: "ERROR",
	FATAL: "FATAL",
}

// Variants of logging output
const (
	JSON Output = iota
	XML
	TEXT
)

// Levels of logging. Defines as prefix
const (
	TRACE Level = iota
	DEBUG
	INFO
	WARN
	ERROR
	FATAL
)

type Logger struct {
	filename string
	*zap.Logger
	bot          *tgbotapi.BotAPI
	currentLevel Level
	output       Output
	id           int64
}

type LogLevelStruct struct {
	Lvl       string      `json:"level" xml:"level"`
	Timestamp string      `json:"timestamp" xml:"timestamp"`
	Message   interface{} `json:"message" xml:"message"`
}

/*
NewConsoleLogger method creates logger with standard level and output
with printing to console
*/
func NewConsoleLogger(defaultLevel Level, defaultOutput Output, token string, id int64) *Logger {
	loggerConfig := zap.NewProductionConfig().EncoderConfig
	loggerConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	loggerConfig.EncodeCaller = zapcore.FullCallerEncoder
	core := zapcore.NewCore(zapcore.NewJSONEncoder(loggerConfig), zapcore.AddSync(os.Stdout), zapcore.InfoLevel)
	bot := &tgbotapi.BotAPI{}
	if token != "" {
		bot, _ = tgbotapi.NewBotAPI(token)
	}
	return &Logger{
		output:       defaultOutput,
		currentLevel: defaultLevel,
		Logger:       zap.New(core),
		bot:          bot,
		id:           id,
	}
}

/*
NewFileConsoleLogger method creates logger with standard level, output and time to recreate logger file
with printing to file and console
Pattern time.RFC822 is using for creation new logging file.
*/
func NewFileConsoleLogger(defaultLevel Level, filename string, amountTimeToRecreate int) (*Logger, error) {
	filename = createLogFileNameWithDate(filename)
	file, fileErr := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if fileErr != nil {
		return nil, fileErr
	}
	writeSyncer := zapcore.AddSync(file)
	//bot, _ := new("5790872034:AAHx-OJoIXQfA2XmAguVQ7QVf0FSjZ1v-NY")
	bot, _ := tgbotapi.NewBotAPI("5790872034:AAHx-OJoIXQfA2XmAguVQ7QVf0FSjZ1v-NY")
	l := &Logger{
		filename:     filename,
		currentLevel: defaultLevel,
		Logger:       zap.New(zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewProductionConfig().EncoderConfig), writeSyncer, zapcore.InfoLevel)),
		bot:          bot,
	}
	go l.recreateDefaultLoggerFile(amountTimeToRecreate)
	return l, nil
}

/*
createLogFileNameWithDate using for create file with filename based on current time and filename
*/
func createLogFileNameWithDate(filename string) string {
	format := strings.Split(filename, ".")
	fileFormat := format[len(format)-1]
	resultFileName := strings.Join(format[:len(format)-1], "")
	resultDate := strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(time.Now().Format(time.RFC822), " ", "_"), "+", ""), ":", "_")
	return fmt.Sprintf("%s_%s.%s", resultFileName, resultDate, fileFormat)
}

/*
recreateDefaultLoggerFile with amount input as time in hours for recreation files in the background
using goroutine thread
*/
func (l *Logger) recreateDefaultLoggerFile(amount int) {
	for {
		time.Sleep(time.Duration(amount) * time.Hour)
		resultFileName := createLogFileNameWithDate(l.filename)
		file, _ := os.OpenFile(resultFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		writer := zapcore.AddSync(file)
		zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewProductionConfig().EncoderConfig), writer, zapcore.InfoLevel)
	}
}

// Log for logging with standard level which passed using NewConsoleLogger or NewFileConsoleLogger functions
func (l Logger) Log(message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(l.currentLevel, l.output, message))
}

// LogCtx for logging with X-Request-ID and standard level which passed using NewConsoleLogger or NewFileConsoleLogger functions
func (l Logger) LogCtx(ctx context.Context, message interface{}) {
	//l.logger.Info(printBasedOnOutputTypeAndLevel(l.currentLevel, l.output, &LogModel{
	//	RequestID: extractRequestIDFromContext(ctx),
	//	Message:   message,
	//}))
	l.Logger.Info("log", zap.String("X-Request-ID", extractRequestIDFromContext(ctx)), zap.Reflect("msg", message))
}

// LogWithLevel with custom level
func (l Logger) LogWithLevel(lvl Level, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(lvl, l.output, message))
}

// LogWithLevelCtx with custom level and X-Request-ID
func (l Logger) LogWithLevelCtx(ctx context.Context, lvl Level, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(lvl, l.output, &LogModel{
		RequestID: extractRequestIDFromContext(ctx),
		Message:   message,
	}))
}

// Trace level logging
func (l Logger) Trace(message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(TRACE, l.output, message))
}

// TraceCtx level logging with X-Request-ID
func (l Logger) TraceCtx(ctx context.Context, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(TRACE, l.output, &LogModel{
		RequestID: extractRequestIDFromContext(ctx),
		Message:   message,
	}))
}

// Debug level logging
func (l Logger) Debug(message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(DEBUG, l.output, message))
}

// DebugCtx level logging with X-Request-ID
func (l Logger) DebugCtx(ctx context.Context, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(DEBUG, l.output, &LogModel{
		RequestID: extractRequestIDFromContext(ctx),
		Message:   message,
	}))
}

// Info level logging

// InfoCtx level logging with X-Request-ID
func (l Logger) InfoCtx(ctx context.Context, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(INFO, l.output, &LogModel{
		RequestID: extractRequestIDFromContext(ctx),
		Message:   message,
	}))
}

// Warn level logging
func (l Logger) Warn(message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(WARN, l.output, message))
	if l.id != 0 {
		l.bot.Send(tgbotapi.NewMessage(l.id, printBasedOnOutputTypeAndLevel(WARN, l.output, message)))
	}

}

// WarnCtx level logging with X-Request-ID
func (l Logger) WarnCtx(ctx context.Context, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(WARN, l.output, &LogModel{
		RequestID: extractRequestIDFromContext(ctx),
		Message:   message,
	}))
	if l.id != 0 {
		l.bot.Send(tgbotapi.NewMessage(l.id, printBasedOnOutputTypeAndLevel(WARN, l.output, &LogModel{
			RequestID: extractRequestIDFromContext(ctx),
			Message:   message,
		})))
	}
}

// Error level logging
func (l Logger) Error(message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(ERROR, l.output, message))
	if l.id != 0 {
		l.bot.Send(tgbotapi.NewMessage(l.id, printBasedOnOutputTypeAndLevel(ERROR, l.output, message)))
	}
}

// ErrorCtx level logging with X-Request-ID
func (l Logger) ErrorCtx(ctx context.Context, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(ERROR, l.output, &LogModel{
		RequestID: extractRequestIDFromContext(ctx),
		Message:   message,
	}))
	if l.id != 0 {
		l.bot.Send(tgbotapi.NewMessage(l.id, printBasedOnOutputTypeAndLevel(ERROR, l.output, &LogModel{
			RequestID: extractRequestIDFromContext(ctx),
			Message:   message,
		})))
	}
}

// Fatal level logging
func (l Logger) Fatal(message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(FATAL, l.output, message))
	if l.id != 0 {
		l.bot.Send(tgbotapi.NewMessage(l.id, printBasedOnOutputTypeAndLevel(FATAL, l.output, message)))
	}
}

// FatalCtx level logging with X-Request-ID
func (l Logger) FatalCtx(ctx context.Context, message interface{}) {
	l.Logger.Info(printBasedOnOutputTypeAndLevel(FATAL, l.output, &LogModel{
		RequestID: extractRequestIDFromContext(ctx),
		Message:   message,
	}))
	if l.id != 0 {
		l.bot.Send(tgbotapi.NewMessage(l.id, printBasedOnOutputTypeAndLevel(FATAL, l.output, &LogModel{
			RequestID: extractRequestIDFromContext(ctx),
			Message:   message,
		})))
	}
}

// printBasedOnOutputTypeAndLevel is printing messages based on Output format
func printBasedOnOutputTypeAndLevel(l Level, o Output, message interface{}) string {
	switch o {
	case JSON:
		return printJson(l, message)
	case XML:
		return printXml(l, message)
	case TEXT:
		return fmt.Sprintf("%s Timestamp: %s Message: %s", m[l], time.Now().Format(time.RFC3339Nano), message)
	default:
		return printJson(l, message)
	}
}

// printXml helper function for printing Output XML format
func printXml(l Level, message interface{}) string {
	logLvl := &LogLevelStruct{
		Lvl:       m[l],
		Timestamp: time.Now().Format(time.RFC3339Nano),
		Message:   message,
	}
	data, _ := xml.Marshal(logLvl)
	return fmt.Sprintf("%s", data)
}

// printJson helper function for printing Output JSON format
func printJson(l Level, message interface{}) string {
	logLvl := &LogLevelStruct{
		Lvl:       m[l],
		Timestamp: time.Now().Format(time.RFC3339Nano),
		Message:   message,
	}
	data, _ := json.Marshal(logLvl)
	return fmt.Sprintf("%s", data)
}

func extractRequestIDFromContext(ctx context.Context) string {
	val := ctx.Value(lCtx.RequestIDKey)

	if val == nil {
		return ""
	}
	return fmt.Sprintf("%v", val)
}
