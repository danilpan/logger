package middleware

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/danilpan/logger"
	lCtx "gitlab.com/danilpan/logger/context"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"strings"
)

// RequestIDGenerator for generating request IDs
type RequestIDGenerator func() string

type Logger struct {
	l           *logger.Logger
	generator   RequestIDGenerator
	excludeUrls map[string]interface{}
}

// NewLoggerMiddleware for creating Logger middleware with logger.Logger and several LoggerOption
// if WithCustomGenerator option is not passed - using default one for uuid.UUID generation
func NewLoggerMiddleware(l *logger.Logger, opts ...LoggerOption) *Logger {
	lg := &Logger{l: l}
	for _, v := range opts {
		v(lg)
	}
	if lg.generator == nil {
		lg.generator = func() string {
			return uuid.NewString()
		}
	}
	return lg
}

// HandleEchoLogger is handling logger middleware in echo
func (l Logger) HandleEchoLogger(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		r := c.Request()
		reqBodyBytes, _ := ioutil.ReadAll(r.Body)
		r.Body.Close() //  must close
		r.Body = ioutil.NopCloser(bytes.NewBuffer(reqBodyBytes))

		if _, ok := l.excludeUrls[r.RequestURI]; ok {
			return next(c)
		}
		requestId := r.Header.Get("X-Request-ID")

		if requestId == "" {
			requestId = l.generator()
			r.Header.Set("X-Request-ID", requestId)
		}

		var logRqByte []byte
		if strings.Contains(c.Response().Header().Get("Content-Type"), "application/json") || strings.Contains(c.Response().Header().Get("Content-Type"), "application/xml") {
			bufferReq := new(bytes.Buffer)
			if err := json.Compact(bufferReq, reqBodyBytes); err != nil {
				fmt.Println(err)
			}
			logRqByte = bufferReq.Bytes()
		} else if strings.Contains(c.Response().Header().Get("Content-Type"), "Image/jpeg") || strings.Contains(c.Response().Header().Get("Content-Type"), "Image/jpg") || strings.Contains(c.Response().Header().Get("Content-Type"), "Image/png") {
			logRqByte = []byte("image")
		} else {
			logRqByte = []byte("file")
		}

		if r.ContentLength < 10000 {
			l.l.Info("Reuest", zap.String("X-Request-ID", requestId), zap.String("RequestURI", r.RequestURI),
				zap.String("RequestMethod", r.Method), zap.String("RequestAddress", r.RemoteAddr),
				zap.String("Origin", r.Header.Get("Origin")), zap.ByteString("RequestBody", logRqByte),
				zap.String("RequestURL", r.URL.String()))
		} else {
			l.l.Info("Reuest", zap.String("X-Request-ID", requestId), zap.String("RequestURI", r.RequestURI),
				zap.String("RequestMethod", r.Method), zap.String("RequestAddress", r.RemoteAddr),
				zap.String("Origin", r.Header.Get("Origin")), zap.String("RequestBody", "Body is too large"),
				zap.String("RequestURL", r.URL.String()))
		}

		c.SetRequest(r.WithContext(context.WithValue(r.Context(), lCtx.RequestIDKey, requestId)))
		var log bytes.Buffer
		rwr := NewResponseWriterWrapper(c.Response().Writer)

		c.Response().Writer = rwr
		err := next(c)

		respBodyBytes := log.Bytes()
		r.Body.Close() //  must close
		r.Body = ioutil.NopCloser(bytes.NewBuffer(respBodyBytes))

		var logRsByte []byte
		if strings.Contains(c.Response().Header().Get("Content-Type"), "application/json") {
			bufferResp := new(bytes.Buffer)
			if err := json.Compact(bufferResp, rwr.body.Bytes()); err != nil {
				fmt.Println(err)
			}
			logRsByte = bufferResp.Bytes()
		} else {
			logRsByte = rwr.body.Bytes()
		}

		if r.ContentLength < 10000 {
			l.l.Info("Response", zap.String("X-Request-ID", requestId), zap.Int("Status", c.Response().Status), zap.ByteString("ResponseBody", logRsByte))
			//l.l.Info(logger.CreateResponseLoggerModel(requestId, nil, c.Response().Status))
		} else {
			l.l.Info("Response", zap.String("X-Request-ID", requestId), zap.Int("Status", c.Response().Status), zap.String("ResponseBody", "Body is too large"))

		}
		return err
	}
}

// HandleLogger is handling with standard http.ResponseWriter and http.Request
func (l Logger) HandleLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		reqBodyBytes, _ := ioutil.ReadAll(r.Body)
		r.Body.Close() //  must close
		r.Body = ioutil.NopCloser(bytes.NewBuffer(reqBodyBytes))

		requestId := r.Header.Get("X-Request-Id")
		if requestId == "" {
			requestId = l.generator()
			r.Header.Set("X-Request-Id", requestId)
		}
		if r.ContentLength < 2000 {
			l.l.Info("Reuest", zap.String("X-Request-ID", requestId), zap.String("RequestURI", r.RequestURI),
				zap.String("RequestMethod", r.Method), zap.String("RequestAddress", r.RemoteAddr),
				zap.String("Origin", r.Header.Get("Origin")), zap.ByteString("RequestBody", reqBodyBytes))
		} else {
			l.l.Info("Reuest", zap.String("X-Request-ID", requestId), zap.String("RequestURI", r.RequestURI),
				zap.String("RequestMethod", r.Method), zap.String("RequestAddress", r.RemoteAddr),
				zap.String("Origin", r.Header.Get("Origin")), zap.String("RequestBody", "Body is too large"))
		}
		sr := &StatusRecorder{
			ResponseWriter: rw,
			Status:         200,
		}

		r = r.WithContext(context.WithValue(r.Context(), lCtx.RequestIDKey, requestId))

		next.ServeHTTP(sr, r)

		l.l.Info("Response", zap.String("X-Request-ID", requestId), zap.Int("Status", sr.Status))

	})
}

func NewResponseWriterWrapper(w http.ResponseWriter) ResponseWriterWrapper {
	var buf bytes.Buffer
	var statusCode int = 200
	return ResponseWriterWrapper{
		w:          &w,
		body:       &buf,
		statusCode: &statusCode,
	}
}

func (rww ResponseWriterWrapper) Write(buf []byte) (int, error) {
	rww.body.Write(buf)
	return (*rww.w).Write(buf)
}

// Header function overwrites the http.ResponseWriter Header() function
func (rww ResponseWriterWrapper) Header() http.Header {
	return (*rww.w).Header()

}

// WriteHeader function overwrites the http.ResponseWriter WriteHeader() function
func (rww ResponseWriterWrapper) WriteHeader(statusCode int) {
	(*rww.statusCode) = statusCode
	(*rww.w).WriteHeader(statusCode)
}

type ResponseWriterWrapper struct {
	w          *http.ResponseWriter
	body       *bytes.Buffer
	statusCode *int
}
