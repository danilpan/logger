package middleware

// LoggerOption for modifying Logger middleware
type LoggerOption func(l *Logger)

// WithCustomGenerator option for passing custom ID generator
func WithCustomGenerator(f RequestIDGenerator) LoggerOption {
	return func(l *Logger) {
		l.generator = f
	}
}

func WithExcludeUrls(urls map[string]interface{}) LoggerOption {
	return func(l *Logger) {
		l.excludeUrls = urls
	}
}
