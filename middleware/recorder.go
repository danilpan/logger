package middleware

import (
	"bufio"
	"errors"
	"net"
	"net/http"
)

// StatusRecorder for recording response status
type StatusRecorder struct {
	http.ResponseWriter
	Status int
}

// WriteHeader standard function for http.ResponseWriter with recording status to structure StatusRecorder
func (r *StatusRecorder) WriteHeader(status int) {
	r.Status = status
	r.ResponseWriter.WriteHeader(status)
}

// Hijack function default for http.ResponseWriter
func (r *StatusRecorder) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	h, ok := r.ResponseWriter.(http.Hijacker)
	if !ok {
		return nil, nil, errors.New("hijack not supported")
	}
	return h.Hijack()
}
